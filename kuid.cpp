#include "kuid.h"

Kuid::Kuid()
    :authorId(0), contentId(0), version(0)
{

}

QDataStream& operator <<(QDataStream& stream, const Kuid& kuid) {
    stream << kuid.contentId;
    auto a = static_cast<quint32>(kuid.authorId);
    if (kuid.version != 0) {
        a &= 0x00ffffff;
        a |= static_cast<quint32>(kuid.version << 25);
    }
    stream << a;
    return stream;
}

QDataStream& operator >>(QDataStream& stream, Kuid& kuid) {
    quint32 a;
    stream >> kuid.contentId >> a;
    if (a & 0x80000000) {
        kuid.authorId = static_cast<qint32>(a);
        kuid.version = 0;
    }
    else {
        kuid.authorId = static_cast<qint32>(a & 0x00ffffff);
        kuid.version = a >> 25;
    }
    return stream;
}

QString Kuid::toString() {
    if (version) {
        return QString("<kuid2:%1:%2:%3>").arg(authorId).arg(contentId).arg(version);
    }
    return QString("<kuid:%1:%2>").arg(authorId).arg(contentId);
}
