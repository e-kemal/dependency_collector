#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::readGnd(QFile& file)
{
    QDataStream stream(&file);
    stream.setByteOrder(QDataStream::LittleEndian);
    stream.setFloatingPointPrecision(QDataStream::SinglePrecision);
    quint32 a;
    stream >> a;
    if ((a & 0x00ffffff) != 0x00444E47) {
        ui->textEdit->append(QString("uncknown format"));
        return;
    }
    int version = (a >> 24) & 0x000000ff;
    ui->textEdit->append(QString("GND, v%1").arg(version));
    if (version != 14) {
        ui->textEdit->append(QString("uncknown version"));
        return;
    }

    stream >> a;
    ui->textEdit->append(QString("tile count: %1").arg(a));

    QVector<quint32> tileAddrs(static_cast<int>(a));
    for (quint32 i = 0; i < a; ++i) {
        qint32 x, y;
        quint32 type;
        quint32 addr1, addr2;
        stream >> x >> y >> type >> addr1 >> addr2;
//        ui->textEdit->append(QString("%1:%2").arg(x).arg(y));
        tileAddrs[static_cast<int>(i)] = addr1;
    }

    qint32 b;
    stream >> b;
    ui->textEdit->append(QString("?? -1: %1").arg(b));
    stream >> b;
    ui->textEdit->append(QString("?? 1: %1").arg(b));

    stream >> a;
    ui->textEdit->append(QString("sky point count: %1").arg(a));

    Kuid kuid;
    stream >> kuid;
    ui->textEdit->append(QString("sky: ") + kuid.toString());

    float f;
    stream >> f;
    ui->textEdit->append(QString("weather: %1").arg(static_cast<double>(f)));
    stream >> f;
    ui->textEdit->append(QString("editor time: %1").arg(static_cast<double>(f)));

    for (quint32 i = 0; i < a; ++i) {
        float t;
        stream >> t;
        ui->textEdit->append(QString("time: %1").arg(static_cast<double>(t)));
        float r, g, b, a;
        stream >> r >> g >> b >> a;
//        ui->textEdit->append(QString("%1 %2 %3 %4").arg(static_cast<double>(r)).arg(static_cast<double>(g)).arg(static_cast<double>(b)).arg(static_cast<double>(a)));
        stream >> r >> g >> b >> a;
//        ui->textEdit->append(QString("%1 %2 %3 %4").arg(static_cast<double>(r)).arg(static_cast<double>(g)).arg(static_cast<double>(b)).arg(static_cast<double>(a)));
        stream >> r >> g >> b >> a;
//        ui->textEdit->append(QString("%1 %2 %3 %4").arg(static_cast<double>(r)).arg(static_cast<double>(g)).arg(static_cast<double>(b)).arg(static_cast<double>(a)));
        stream >> r >> g >> b >> a;
//        ui->textEdit->append(QString("%1 %2 %3 %4").arg(static_cast<double>(r)).arg(static_cast<double>(g)).arg(static_cast<double>(b)).arg(static_cast<double>(a)));
        stream >> r >> g >> b >> a;
//        ui->textEdit->append(QString("%1 %2 %3 %4").arg(static_cast<double>(r)).arg(static_cast<double>(g)).arg(static_cast<double>(b)).arg(static_cast<double>(a)));
        stream >> r >> g >> b >> a;
//        ui->textEdit->append(QString("%1 %2 %3 %4").arg(static_cast<double>(r)).arg(static_cast<double>(g)).arg(static_cast<double>(b)).arg(static_cast<double>(a)));
        stream >> r;
//        ui->textEdit->append(QString("%1").arg(static_cast<double>(r)));
    }

    stream >> a;
    ui->textEdit->append(QString("date: %1").arg(QDateTime::fromSecsSinceEpoch(a).toString()));
    stream >> a;
    ui->textEdit->append(QString("0: %1").arg(0));
    stream >> f;
    ui->textEdit->append(QString("wind: %1").arg(static_cast<double>(f)));
    stream >> f;
    ui->textEdit->append(QString("snow: %1").arg(static_cast<double>(f)));
    stream >> f;
    ui->textEdit->append(QString("waterR: %1").arg(static_cast<double>(f)));
    stream >> f;
    ui->textEdit->append(QString("waterG: %1").arg(static_cast<double>(f)));
    stream >> f;
    ui->textEdit->append(QString("waterB: %1").arg(static_cast<double>(f)));
    stream >> f;
    ui->textEdit->append(QString("waterA: %1").arg(static_cast<double>(f)));
    stream >> kuid;
    ui->textEdit->append(QString("water: ") + kuid.toString());
    for (auto i = 0; i < tileAddrs.size(); ++i) {
        stream.device()->seek(tileAddrs[i]);
        quint32 paintSlotCount;
        stream >> paintSlotCount;
        ui->textEdit->append(QString("paintSlotCount: %1").arg(paintSlotCount));
        for (quint32 i = 0; i < paintSlotCount; ++i) {
            quint32 tmp;
            stream >> tmp;
            if (tmp == 0) {
                continue;
            }
            stream >> kuid;
            ui->textEdit->append(QString("paint: ") + kuid.toString());
        }
    }
}

void MainWindow::readObs(QFile &file)
{
    QDataStream stream(&file);
    stream.setByteOrder(QDataStream::LittleEndian);
    stream.setFloatingPointPrecision(QDataStream::SinglePrecision);
    qint32 a;
    stream >> a;
    ui->textEdit->append(QString("?9: %1").arg(a));
    stream >> a;
    ui->textEdit->append(QString("?5: %1").arg(a));
    stream >> a;
    ui->textEdit->append(QString("?9: %1").arg(a));

    stream >> a;
    while (a >= 0) {
        ui->textEdit->append(QString("id: %1").arg(a));

        stream >> a;
        ui->textEdit->append(QString("type: %1").arg(a));

        Kuid kuid;
        stream >> kuid;
        ui->textEdit->append(QString("kuid ") + kuid.toString());

        stream >> a;
        stream.skipRawData(a);
        stream >> a;
    }
    ui->textEdit->append(QString("end: %1").arg(a));
}

void MainWindow::on_pushButton_clicked()
{
    QSettings settings(QString("kemal"), QString("dependency_collector"));
    auto path = QFileDialog::getExistingDirectory(
                this,
                QString(),
                settings.value(QString("last_open_dir"), QString()).toString()
                );
    if (path.isNull())
        return;
    settings.setValue(QString("last_open_dir"), path);
    ui->textEdit->clear();
    ui->textEdit->append("dir: " + path);
    auto directory = QDir(path);
    auto list = directory.entryList(QDir::Files);
    for (auto i = list.begin(); i != list.end(); ++i) {
        ui->textEdit->append(*i);
        auto fileInfo = QFileInfo(directory.absoluteFilePath(*i));
//        ui->textEdit->append(fileInfo.suffix());
        if (!fileInfo.suffix().compare("gnd", Qt::CaseInsensitive)) {
            QFile file(fileInfo.absoluteFilePath());
            file.open(QIODevice::ReadOnly);
            readGnd(file);
            file.close();
        }
        if (!fileInfo.suffix().compare("obs", Qt::CaseInsensitive)) {
            QFile file(fileInfo.absoluteFilePath());
            file.open(QIODevice::ReadOnly);
            readObs(file);
            file.close();
        }
    }
}
