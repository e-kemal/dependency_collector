#ifndef KUID_H
#define KUID_H

#include <QDataStream>

class Kuid
{
    qint32 authorId;
    qint32 contentId;
    quint8 version;
public:
    Kuid();
    friend QDataStream& operator <<(QDataStream& stream, const Kuid& kuid);
    friend QDataStream& operator >>(QDataStream& stream, Kuid& kuid);
    QString toString();
};

#endif // KUID_H
